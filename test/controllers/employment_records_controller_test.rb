require 'test_helper'

class EmploymentRecordsControllerTest < ActionController::TestCase
  setup do
    @employment_record = employment_records(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:employment_records)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create employment_record" do
    assert_difference('EmploymentRecord.count') do
      post :create, employment_record: { company_address: @employment_record.company_address, company_name: @employment_record.company_name, company_position: @employment_record.company_position, date_end: @employment_record.date_end, date_start: @employment_record.date_start }
    end

    assert_redirected_to employment_record_path(assigns(:employment_record))
  end

  test "should show employment_record" do
    get :show, id: @employment_record
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @employment_record
    assert_response :success
  end

  test "should update employment_record" do
    patch :update, id: @employment_record, employment_record: { company_address: @employment_record.company_address, company_name: @employment_record.company_name, company_position: @employment_record.company_position, date_end: @employment_record.date_end, date_start: @employment_record.date_start }
    assert_redirected_to employment_record_path(assigns(:employment_record))
  end

  test "should destroy employment_record" do
    assert_difference('EmploymentRecord.count', -1) do
      delete :destroy, id: @employment_record
    end

    assert_redirected_to employment_records_path
  end
end
