require 'test_helper'

class ProfilesControllerTest < ActionController::TestCase
  setup do
    @profile = profiles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:profiles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create profile" do
    assert_difference('Profile.count') do
      post :create, profile: { citizenship: @profile.citizenship, date_of_birth: @profile.date_of_birth, first_name: @profile.first_name, height: @profile.height, last_name: @profile.last_name, middle_name: @profile.middle_name, mobile: @profile.mobile, permanent_address: @profile.permanent_address, place_of_birth: @profile.place_of_birth, position: @profile.position, religion: @profile.religion, tel: @profile.tel, weight: @profile.weight }
    end

    assert_redirected_to profile_path(assigns(:profile))
  end

  test "should show profile" do
    get :show, id: @profile
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @profile
    assert_response :success
  end

  test "should update profile" do
    patch :update, id: @profile, profile: { citizenship: @profile.citizenship, date_of_birth: @profile.date_of_birth, first_name: @profile.first_name, height: @profile.height, last_name: @profile.last_name, middle_name: @profile.middle_name, mobile: @profile.mobile, permanent_address: @profile.permanent_address, place_of_birth: @profile.place_of_birth, position: @profile.position, religion: @profile.religion, tel: @profile.tel, weight: @profile.weight }
    assert_redirected_to profile_path(assigns(:profile))
  end

  test "should destroy profile" do
    assert_difference('Profile.count', -1) do
      delete :destroy, id: @profile
    end

    assert_redirected_to profiles_path
  end
end
