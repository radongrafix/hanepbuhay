require 'test_helper'

class CharacterReferencesControllerTest < ActionController::TestCase
  setup do
    @character_reference = character_references(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:character_references)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create character_reference" do
    assert_difference('CharacterReference.count') do
      post :create, character_reference: { address: @character_reference.address, name: @character_reference.name, phone: @character_reference.phone, relationship: @character_reference.relationship }
    end

    assert_redirected_to character_reference_path(assigns(:character_reference))
  end

  test "should show character_reference" do
    get :show, id: @character_reference
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @character_reference
    assert_response :success
  end

  test "should update character_reference" do
    patch :update, id: @character_reference, character_reference: { address: @character_reference.address, name: @character_reference.name, phone: @character_reference.phone, relationship: @character_reference.relationship }
    assert_redirected_to character_reference_path(assigns(:character_reference))
  end

  test "should destroy character_reference" do
    assert_difference('CharacterReference.count', -1) do
      delete :destroy, id: @character_reference
    end

    assert_redirected_to character_references_path
  end
end
