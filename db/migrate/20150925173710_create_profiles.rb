class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :position
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.text :permanent_address
      t.string :tel
      t.string :mobile
      t.date :date_of_birth
      t.string :place_of_birth
      t.string :height
      t.string :weight
      t.string :religion
      t.string :citizenship

      t.timestamps null: false
    end
  end
end
