class CreateEmploymentRecords < ActiveRecord::Migration
  def change
    create_table :employment_records do |t|
      t.string :company_name
      t.string :company_address
      t.string :company_position
      t.string :date_start
      t.string :date_end

      t.timestamps null: false
    end
  end
end
