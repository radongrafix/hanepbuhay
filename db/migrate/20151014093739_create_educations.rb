class CreateEducations < ActiveRecord::Migration
  def change
    create_table :educations do |t|
      t.string :school_name
      t.string :degree_receive
      t.string :year_attended

      t.timestamps null: false
    end
  end
end
