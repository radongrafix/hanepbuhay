class AddUserIdToCharacterReferences < ActiveRecord::Migration
  def change
    add_column :character_references, :user_id, :integer
  end
end
