class AddGenderToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :gender, :integer
    add_column :profiles, :status, :integer
  end
end
