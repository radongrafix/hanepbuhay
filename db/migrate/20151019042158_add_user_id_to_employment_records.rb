class AddUserIdToEmploymentRecords < ActiveRecord::Migration
  def change
    add_column :employment_records, :user_id, :integer
  end
end
