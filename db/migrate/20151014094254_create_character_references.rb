class CreateCharacterReferences < ActiveRecord::Migration
  def change
    create_table :character_references do |t|
      t.string :name
      t.string :address
      t.string :relationship
      t.string :phone

      t.timestamps null: false
    end
  end
end
