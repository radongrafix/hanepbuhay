class AddProfileIdToCharacterReferences < ActiveRecord::Migration
  def change
    add_reference :character_references, :profile, index: true, foreign_key: true
  end
end
