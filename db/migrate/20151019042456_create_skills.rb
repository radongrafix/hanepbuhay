class CreateSkills < ActiveRecord::Migration
  def change
    create_table :skills do |t|
      t.string :skill_name
      t.references :profile, index: true, foreign_key: true
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
