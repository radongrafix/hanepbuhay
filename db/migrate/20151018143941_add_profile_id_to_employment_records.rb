class AddProfileIdToEmploymentRecords < ActiveRecord::Migration
  def change
    add_reference :employment_records, :profile, index: true, foreign_key: true
  end
end
