class EmploymentRecordsController < ApplicationController
  before_action :set_employment_record, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /employment_records
  # GET /employment_records.json
  def index
    @employment_records = EmploymentRecord.all
  end

  # GET /employment_records/1
  # GET /employment_records/1.json
  def show
  end

  # GET /employment_records/new
  def new
    # @employment_record = EmploymentRecord.new
    @employment_record = current_user.employment_records.build
  end

  # GET /employment_records/1/edit
  def edit
  end

  # POST /employment_records
  # POST /employment_records.json
  def create
    # @employment_record = EmploymentRecord.new(employment_record_params)
    @employment_record = current_user.employment_records.build(employment_record_params)
    respond_to do |format|
      if @employment_record.save
        format.html { redirect_to @employment_record, notice: 'Employment record was successfully created.' }
        format.json { render :show, status: :created, location: @employment_record }
      else
        format.html { render :new }
        format.json { render json: @employment_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employment_records/1
  # PATCH/PUT /employment_records/1.json
  def update
    respond_to do |format|
      if @employment_record.update(employment_record_params)
        format.html { redirect_to @employment_record, notice: 'Employment record was successfully updated.' }
        format.json { render :show, status: :ok, location: @employment_record }
      else
        format.html { render :edit }
        format.json { render json: @employment_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employment_records/1
  # DELETE /employment_records/1.json
  def destroy
    @employment_record.destroy
    respond_to do |format|
      format.html { redirect_to employment_records_url, notice: 'Employment record was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employment_record
      @employment_record = EmploymentRecord.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employment_record_params
      params.require(:employment_record).permit(:company_name, :company_address, :company_position, :date_start, :date_end, :profile_id)
    end
end
