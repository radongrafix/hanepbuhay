class CharacterReferencesController < ApplicationController
  before_action :set_character_reference, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /character_references
  # GET /character_references.json
  def index
    @character_references = CharacterReference.all
  end

  # GET /character_references/1
  # GET /character_references/1.json
  def show
  end

  # GET /character_references/new
  def new
    # @character_reference = CharacterReference.new
    @character_reference = current_user.character_references.build
  end

  # GET /character_references/1/edit
  def edit
  end

  # POST /character_references
  # POST /character_references.json
  def create
    # @character_reference = CharacterReference.new(character_reference_params)
    @character_reference = current_user.character_references.build(character_reference_params)

    respond_to do |format|
      if @character_reference.save
        format.html { redirect_to @character_reference, notice: 'Character reference was successfully created.' }
        format.json { render :show, status: :created, location: @character_reference }
      else
        format.html { render :new }
        format.json { render json: @character_reference.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /character_references/1
  # PATCH/PUT /character_references/1.json
  def update
    respond_to do |format|
      if @character_reference.update(character_reference_params)
        format.html { redirect_to @character_reference, notice: 'Character reference was successfully updated.' }
        format.json { render :show, status: :ok, location: @character_reference }
      else
        format.html { render :edit }
        format.json { render json: @character_reference.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /character_references/1
  # DELETE /character_references/1.json
  def destroy
    @character_reference.destroy
    respond_to do |format|
      format.html { redirect_to character_references_url, notice: 'Character reference was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_character_reference
      @character_reference = CharacterReference.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def character_reference_params
      params.require(:character_reference).permit(:name, :address, :relationship, :phone, :profile_id)
    end
end
