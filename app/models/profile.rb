class Profile < ActiveRecord::Base
	belongs_to :user
	has_many :educations, dependent: :destroy
	has_many :character_references, dependent: :destroy
	has_many :employment_records, dependent: :destroy
	has_many :skills, dependent: :destroy

	enum gender: [:male, :female]
	enum status: [:single, :married, :divorce, :widow]

	accepts_nested_attributes_for :educations
	accepts_nested_attributes_for :character_references
	
	mount_uploader :avatar, AvatarUploader
end
