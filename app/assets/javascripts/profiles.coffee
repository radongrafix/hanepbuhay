jQuery ->
  $('#profile_date_of_birth').datepicker
    dateFormat: 'yyyy-mm-dd',
    changeMonth: true,
    changeYear: true,
    yearRange: "-80:+0",
