json.array!(@educations) do |education|
  json.extract! education, :id, :school_name, :degree_receive, :year_attended
  json.url education_url(education, format: :json)
end
