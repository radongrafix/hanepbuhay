json.array!(@character_references) do |character_reference|
  json.extract! character_reference, :id, :name, :address, :relationship, :phone
  json.url character_reference_url(character_reference, format: :json)
end
