json.array!(@jobs) do |job|
  json.extract! job, :id, :title, :description, :how_many
  json.url job_url(job, format: :json)
end
