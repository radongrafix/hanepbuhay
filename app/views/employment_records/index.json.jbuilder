json.array!(@employment_records) do |employment_record|
  json.extract! employment_record, :id, :company_name, :company_address, :company_position, :date_start, :date_end
  json.url employment_record_url(employment_record, format: :json)
end
