json.array!(@profiles) do |profile|
  json.extract! profile, :id, :position, :first_name, :middle_name, :last_name, :permanent_address, :tel, :mobile, :date_of_birth, :place_of_birth, :height, :weight, :religion, :citizenship
  json.url profile_url(profile, format: :json)
end
